# Message delivery subsystem
Your task is to design a simple as possible yet scalable and performant service that would be responsible for delivery of JSON messages to our business partners that integrate with our services. In other words - a service that invokes 3rd party webhooks.

## Requirements for the solution

 - Create a high level architecture diagram(s) of the subsystem. Include details where they are necessary.
 - Diagram(s) should explain what components the system will consist of and how they interact with each other.
 - Don't forget to add a basic description of persistence layer models and their relationships if such layer is present in the solution. Do not list all attributes, just the important ones.
 - Include a short description that would help us understand the context and details that might not be immediately visible in the diagram(s) if such description is necessary.
 - Use any tool(s) that is convenient to You to create the documentation. Do not forget that the solution should be provided in a popular format (i.e. PDF, DOCX, etc.)

## Requirements for the subsystem you will design

- Internal API to receive messages that have to get delivered to interested subscribers ASAP.
- Public API for 3rd parties to subscribe to specific message types they are interested in. When subscribing, 3rd party must specify webhook url to be invoked together with necessary credentials and authentication method.
- Mechanism for message delivery. Failed webhook invocations should be retried based on configuration (max retries, interval, exponential backoff, etc.). 
- The system must maintain a log of webhook invocations.
- The system should be able hold up to 200 events per second at peak time.
- Using a public API 3rd parties should be able to browse the invocation log of messages they have subscribed to.

#### Good luck !
